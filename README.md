 <div align="center">
   <img src="https://www.awesomity.rw/images/IB_logo.png" alt="App brand icon" width="80" />
 </div>
 <br />

# TodoApp

A React Native based To-Do app to manage daily tasks.
<table>
  <tr>
    <td>
      <img src="https://res.cloudinary.com/elvis-rugamba/image/upload/v1645711879/Awesomity/1645711020572_100_blb5di.png">
    </td>
    <td>
      <img src="https://res.cloudinary.com/elvis-rugamba/image/upload/v1645711881/Awesomity/1645711052465_100_koaexp.png">
    </td>
    <td>
      <img src="https://res.cloudinary.com/elvis-rugamba/image/upload/v1645711882/Awesomity/1645711068589_100_lqoo2u.png">
    </td>
    <td>
      <img src="https://res.cloudinary.com/elvis-rugamba/image/upload/v1645711881/Awesomity/1645711083339_100_iug6pt.png">
    </td>
  </tr>
</table>

## Prerequisites

- [Node.js > 12](https://nodejs.org) and npm (Recommended: Use [nvm](https://github.com/nvm-sh/nvm))
- [JDK > 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Android Studio and Android SDK](https://developer.android.com/studio)

## Base dependencies

- [react-native](https://reactnative.dev/)
- [react-navigation](https://reactnavigation.org/) navigation library.
- [react-native-async-storage](https://react-native-async-storage.github.io/async-storage/) as storage solution.
- [redux](https://redux.js.org/) for state management.
- [redux-persist](https://github.com/rt2zz/redux-persist) as persistance layer.
- [redux-thunk](https://github.com/gaearon/redux-thunk) to dispatch asynchronous actions.
- [jest](https://facebook.github.io/jest/) and [react-native-testing-library](https://callstack.github.io/react-native-testing-library/) for testing.

## Installation

If you want to run the App locally, follow these instructions:

Clone this repository into your local machine

```
git clone https://gitlab.com/elvisrugamba/TodoApp.git
```
```
cd TodoApp
```
Install dependencies 
```
npm install
```
Start metro bundler

```
npm start
``` 
Open new terminal and start the application on Android by running the script

```
npm run android
``` 

## Running Tests

To run tests, run the following command

```
  npm run test
```

## Author

- [Elvis Rugamba](https://github.com/Elvis-rugamba)

## Acknowledgements

- [Awesomity Lab](https://www.awesomity.rw/)
- [Code of Africa](https://www.codeofafrica.com/EN)

